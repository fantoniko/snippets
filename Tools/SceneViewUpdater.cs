﻿#if UNITY_EDITOR
 
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class SceneViewUpdater
{
    static Bounds SceneViewBounds;

    static SceneViewUpdater()
    {
        EditorApplication.update += Update;
    }

    public static bool IsPointInsideSceneView(Vector3 position)
    {
        return SceneViewBounds.Contains(position);
    }

    static void Update()
    {
        UpdateSceneViewBounds();
    }

    static void UpdateSceneViewBounds()
    {
        var camera = SceneView.lastActiveSceneView?.camera;
        if (!camera)
        {
            return;
        }
        
        var leftBottom = camera.ViewportToWorldPoint(Vector2.zero);
        var rightUp = camera.ViewportToWorldPoint(Vector2.one);

        leftBottom.z = 0f;
        rightUp.z = 0f;

        var center = (rightUp + leftBottom) * 0.5f;
        var extents = rightUp - leftBottom;

        SceneViewBounds = new Bounds(center, extents);
    }
}
#endif
