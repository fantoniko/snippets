using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class EditorEnvironment
{
    const string DuplicateEventName = "Duplicate";
    const string PasteEventName = "Paste";

    static readonly List<string> ValidationEvents = new List<string>() { DuplicateEventName, PasteEventName };

    public static bool LastEventWasDuplication()
    {
        Event e = Event.current;
        
        return e != null
               && e.type == EventType.ExecuteCommand
               && ValidationEvents.Contains(e.commandName);
    }
    
}
