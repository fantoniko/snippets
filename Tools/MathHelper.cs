﻿using UnityEngine;


public struct MathHelper
{
    /// <summary>
    /// Rotate Vector2 on the angel in degrees
    /// </summary>
    /// <param name="original">Rotating vector</param>
    /// <param name="angle">Angel in degrees</param>
    /// <returns>Rotated vector</returns>
    public static Vector2 RotateVector(Vector2 original, float angle)
    {
        var rad = Mathf.Deg2Rad * angle;
        return new Vector2(
            original.x * Mathf.Cos(rad) - original.y * Mathf.Sin(rad),
            original.x * Mathf.Sin(rad) + original.y * Mathf.Cos(rad)
            );
    }
}
