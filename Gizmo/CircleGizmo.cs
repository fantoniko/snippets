﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CircleGizmo : MonoBehaviour
{
    #if UNITY_EDITOR
    [SerializeField] float Radius = 1f;

    private void OnDrawGizmos()
    {
        if (!SceneViewUpdater.IsPointInsideSceneView(transform.position))
        {
            return;
        }

        Gizmos.DrawSphere(transform.position, Radius);
    }
    #endif
}
