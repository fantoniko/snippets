using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorRootContainer : MonoBehaviour
{
    [SerializeField] GameObject RootGo;

    public GameObject Root => RootGo ? RootGo : gameObject;
}

public static class RootContainerColliderExtensions
{
    public static bool TryGetRoot(this Collider collider, out GameObject root)
    {
        var result = collider.TryGetComponent(out ActorRootContainer rootLocalVar);
        root = rootLocalVar?.Root;
        return result;
    }
}
