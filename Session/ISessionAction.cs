﻿using System.Collections.Generic;
using UnityEngine;

public interface ISessionCondition
{
    bool IsMet(SessionEntity session);
}

public interface ISessionAction
{
    void Handle(SessionEntity session);
}