# Unity Snippets

Repository for some kind of Miscellaneous Unity scripts.

Features:
* Environment Mover - GameDesigner's tool for setting determined movement for Container(GameObject) by waypoints and calling generic Game Actions (ISessionAction), all within Unity Inspector window, powered by Odin Inspector and Serializer.
* Different dependencies I had to pull from actual project.

Dependencies:
* Odin Inspector/Serializer (works with 3.1.4.0 version)

TODO:
* EnvironmentMover class must be split to different classes, at least Editor Tools need to be cut out, but for now it's configured as regions in single class.
* Sort code to different namespaces.
