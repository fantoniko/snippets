﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GameObjectExtensions
{
    public static GameObject Create(string name, Transform parent)
    {
        var go = new GameObject(name);
        go.transform.SetParent(parent);
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        return go;
    }
    
    public static T GetOrAddComponent<T>(this Component go) where T : Component
    {
        var component = go.GetComponent<T>();
        if (!component)
        {
            component = go.gameObject.AddComponent<T>();
        }
        return component;
    }

    public static T GetOrAddComponent<T>(this GameObject go) where T : Component
    {
        var component = go.GetComponent<T>();
        if (!component)
        {
            component = go.gameObject.AddComponent<T>();
        }
        return component;
    }

    public static void SetLayerRecursively(this GameObject go, int layer)
    {
        foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = layer;
        }
    }

    public static IEnumerable<T> FindObjectOfTypeScene<T>() where T : MonoBehaviour
    {
        #if UNITY_EDITOR
        return Resources.FindObjectsOfTypeAll<T>()
                        .Where(o => 
                        { 
                            var status = UnityEditor.PrefabUtility.GetPrefabInstanceStatus(o.gameObject);
                            var type = UnityEditor.PrefabUtility.GetPrefabAssetType(o.gameObject);
                            var result = status == UnityEditor.PrefabInstanceStatus.NotAPrefab && type == UnityEditor.PrefabAssetType.NotAPrefab;
                            result |= status != UnityEditor.PrefabInstanceStatus.NotAPrefab && type != UnityEditor.PrefabAssetType.NotAPrefab;
                            return result;
                        });
        #else
        return null;
        #endif
    }
}
