using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollisionTriggerEnter
{
    void HandleEnter(CollisionTrigger trigger, Collider collision, GameObject root);
}

public interface ICollisionTriggerExit
{
    void HandleExit(CollisionTrigger trigger, Collider collision, GameObject root);
}


public class CollisionTrigger : MonoBehaviour
{
    public event Action<CollisionTrigger> OnDestroyed;
    
    Collider CachedCollider;
    public Collider Collider
    {
        get
        {
            if (!CachedCollider)
            {
                CachedCollider = GetComponent<Collider>();
            }
            return CachedCollider;
        }
    }

    List<ICollisionTriggerEnter> EnterHandlers = new List<ICollisionTriggerEnter>();
    List<ICollisionTriggerExit> ExitHandlers = new List<ICollisionTriggerExit>();

    void OnTriggerEnter(Collider collider)
    {
        if (!collider.TryGetRoot(out GameObject root))
            return;
        
        foreach (var handler in EnterHandlers)
        {
            handler.HandleEnter(this, collider, root);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (!collider.TryGetRoot(out GameObject root))
            return;
        
        foreach (var handler in ExitHandlers)
        {
            handler.HandleExit(this, collider, root);
        }
    }

    void OnDestroy()
    {
        OnDestroyed?.Invoke(this);
    }

    public void AddHandlers(object handler)
    {
        AddHandlerEnter((ICollisionTriggerEnter)handler);
        AddHandlerExit((ICollisionTriggerExit)handler);
    }

    public void AddHandlerEnter(ICollisionTriggerEnter handlerEnter)
    {
        EnterHandlers.Add(handlerEnter);
    }

    public void AddHandlerExit(ICollisionTriggerExit handlerExit)
    {
        ExitHandlers.Add(handlerExit);
    }

    public void RemoveHandlers(object handler)
    {
        RemoveHandlerEnter((ICollisionTriggerEnter)handler);
        RemoveHandlerExit((ICollisionTriggerExit)handler);
    }

    public void RemoveHandlerEnter(ICollisionTriggerEnter handlerEnter)
    {
        EnterHandlers.Remove(handlerEnter);
    }

    public void RemoveHandlerExit(ICollisionTriggerExit handlerExit)
    {
        ExitHandlers.Remove(handlerExit);
    }

    public void Enable()
    {
        SetState(true);
    }

    public void Disable()
    {
        SetState(false);
    }

    void SetState(bool state)
    {
        enabled = state;
        if (Collider)
        {
            Collider.enabled = state;
        }
    }
}
