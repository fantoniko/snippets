﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class EnvironmentMoverWaypoint : SerializedMonoBehaviour
{
    public ISessionAction OnStartAction;
    public ISessionAction OnReachedAction;

#if UNITY_EDITOR

    [HideInInspector] public float Rotation;
    [HideInInspector] public int NumberInOrder;
    [HideInInspector] public EnvironmentMoverWaypoint Previous;
    [HideInInspector] public EnvironmentMoverWaypoint Next;

    public Vector3 Position => transform.position;
    public bool IsInEvenPath => transform.parent.GetSiblingIndex() % 2 == 0;
    
    public void SetOrder(int index, EnvironmentMoverWaypoint previous, EnvironmentMoverWaypoint next)
    {
        NumberInOrder = index + 1;
        Previous = previous;
        Next = next;
    }

    public void SetRotation(float rotation)
    {
        Rotation = rotation;
    }
    
    void OnDrawGizmos()
    {
        if (!SceneViewUpdater.IsPointInsideSceneView(Position))
        {
            return;
        }

        var cachedGizmosColor = Gizmos.color;

        var iconPosition = Position;
        Gizmos.DrawIcon(iconPosition, "EnvironmentMover/MoverWaypointIcon.png", true);
        
        var point1 = Position;
        var rotation = Quaternion.Euler(0f, 0f, Rotation);
        var direction = rotation * Vector2.up;
        Gizmos.color = Color.green;
        Gizmos.DrawLine(point1, point1 + direction);

        var isEvenPath = IsInEvenPath;
        var axisOffset = 5f; /*= B.Editor.Gizmos.Mover.WaypointRadius;*/ 
        var verticalCoef = isEvenPath ? 1 : -1;

        var style = GUIStyle.none; /*= B.Editor.Gizmos.Mover.GetFontStyle(isEvenPath);*/

        var offset = new Vector3(axisOffset, axisOffset * verticalCoef);
        Handles.Label(Position + offset, NumberInOrder.ToString(), style);

        Gizmos.color = cachedGizmosColor;
    }

    void OnDrawGizmosSelected()
    {
        var cachedGizmosColor = Gizmos.color;
        
        var waypointPosition = Position;

        if (Previous)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(waypointPosition, Previous.Position);    
        }
        
        if (Next)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(waypointPosition, Next.Position);  
        }

        Gizmos.color = cachedGizmosColor;
    }
#endif

}
