using System;
using System.Collections;
using System.Collections.Generic;
using Game.Environment.Mover;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[Serializable]
public struct WaypointInfo
{
    [GUIColor(0.4f, 0.8f, 1f)]
    public Transform Transform;

#if UNITY_EDITOR
    [OnValueChanged(nameof(UpdateWaypointRotationGizmo))]
#endif
    [GUIColor(0f, 0.8f, 0f)]
    public float Rotation;

    [GUIColor(0.9f, 0.9f, 0f)]
    public float MoveTime;

    public bool UseVelocityCurve;

    [ShowIf(nameof(UseVelocityCurve))]
    public AnimationCurve VelocityCurve;

    [GUIColor(0.9f, 0.3f, 0f)]
    public float StayTime;

    [Header("Deprecated")]
    [ShowInInspector, OdinSerialize] public ISessionAction OnStartAction;

    [ShowInInspector, OdinSerialize] public ISessionAction OnReachedAction;

#if UNITY_EDITOR
    void UpdateWaypointRotationGizmo()
    {
        EnvironmentMoverPath.GetOrAddWaypoint(Transform).SetRotation(Rotation);
    }
#endif
}
