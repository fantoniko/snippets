using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public abstract class IdentifiableMonoBehaviour : SerializedMonoBehaviour
{
    [SerializeField, ReadOnly] string InternalId;

    public virtual string Id => InternalId;

#if UNITY_EDITOR
    void OnValidate()
    {
        if (string.IsNullOrEmpty(Id) 
            || (EditorEnvironment.LastEventWasDuplication() && !IsUnique()))
        {
            GenerateId();
        }
    }
    
    bool IsUnique()
    {
        var otherIdentifiables = GameObjectExtensions.FindObjectOfTypeScene<IdentifiableMonoBehaviour>();
        var otherSpawnPointsList = new List<IdentifiableMonoBehaviour>(otherIdentifiables);
        otherSpawnPointsList.Remove(this);

        foreach (var point in otherSpawnPointsList)
        {
            if (point.Id == Id)
            {
                return false;
            }
        }
        return true;
    }

    [ContextMenu("Generate ID")]
    void GenerateId()
    {
        InternalId = GUID.Generate().ToString();
    }
#endif
    
}
