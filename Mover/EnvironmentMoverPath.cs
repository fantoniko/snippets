﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Game.Environment.Mover
{
    [Serializable, GUIColor(0.8f, 0.9f, 1f)]
    public struct EnvironmentMoverPath
    {
        const string WaypointNamePrefix = "Move ";
        
        public EnvironmentMoverPath(Transform waypointsContainer, bool playOnStart, int loopTimes)
        {
            WaypointsContainer = waypointsContainer;
            PlayOnStart = playOnStart;
            LoopCount = loopTimes;
            Waypoints = new List<WaypointInfo>();
        }

        [HideInInspector] public Transform WaypointsContainer;

        public bool PlayOnStart;
        public int LoopCount;
        
#if UNITY_EDITOR
        [OnValueChanged(nameof(ValidateWaypoints))]
#endif
        public List<WaypointInfo> Waypoints;
        
        public static EnvironmentMoverWaypoint GetOrAddWaypoint(Component component)
        {
            return component.GetOrAddComponent<EnvironmentMoverWaypoint>();
        }

#if UNITY_EDITOR
        public void AddDefaultWaypoints()
        {
            AddWaypoint();
            AddWaypoint();
            Waypoints[1].Transform.localPosition = new Vector3(3f, 0f, 0f);
        }
        
        public void UpdateWaypointsGizmos()
        {
            if (Waypoints.Count > 0)
            {
                if (Waypoints.Count > 1)
                {
                    for (int i = 0; i < Waypoints.Count; i++)
                    {
                        var waypointInfo = Waypoints[i];
                        var waypoint = GetOrAddWaypoint(waypointInfo.Transform);
                        var prevIndex = i > 0 ? i - 1 : Waypoints.Count - 1;
                        var nextIndex = i < Waypoints.Count - 1 ? i + 1 : 0;
                        waypoint.SetOrder(i, 
                            GetOrAddWaypoint(Waypoints[prevIndex].Transform), 
                            GetOrAddWaypoint(Waypoints[nextIndex].Transform));

                        DestroyCircleGizmo(waypoint.transform);
                    }
                }
                else
                {
                    var waypoint = GetOrAddWaypoint(Waypoints[0].Transform);
                    waypoint.SetOrder(0, null, null);
                    DestroyCircleGizmo(waypoint.transform);
                }
            }
        }

        void DestroyCircleGizmo(Transform waypoint)
        {
            if (waypoint.TryGetComponent(out CircleGizmo circleGizmo))
            {
                GameObject.DestroyImmediate(circleGizmo);
            }
        }
        
        void ValidateWaypoints()
        {
            RemoveBadWaypointsFromScene();
            UpdateWaypointsGizmos();
            UpdateWaypointsOrderAndNames();
        }

        void RemoveBadWaypointsFromScene()
        {
            var sceneWaypointsToRemove = new List<GameObject>();
            var validWaypoints = Waypoints.Select(w => w.Transform);
            foreach (Transform waypoint in WaypointsContainer)
            {
                if (!validWaypoints.Contains(waypoint))
                {
                    sceneWaypointsToRemove.Add(waypoint.gameObject);
                }
            }
            
            foreach (var removed in sceneWaypointsToRemove)
            {
                GameObject.DestroyImmediate(removed);    
            }
        }

        void UpdateWaypointsOrderAndNames()
        {
            var waypointsTransforms = Waypoints.Select(w => w.Transform).ToArray();
            for (int i = 0; i < waypointsTransforms.Length; i++)
            {
                waypointsTransforms[i].SetSiblingIndex(i);
                
                var waypointIndice = i + 1;
                waypointsTransforms[i].name = WaypointNamePrefix + waypointIndice;
            }
        }

        [Button(ButtonHeight = 20, Name = nameof(AddWaypoint)), GUIColor(0f, 0.8f, 0f)]
        void AddWaypoint()
        {
            var waypointIndice = WaypointsContainer.childCount + 1;
            var waypointGO = GameObjectExtensions.Create(WaypointNamePrefix + waypointIndice, WaypointsContainer);

            var waypoint = waypointGO.AddComponent<EnvironmentMoverWaypoint>();
            var waypointInfo = new WaypointInfo
            {
                Transform = waypoint.transform,
                MoveTime = 1f,
                VelocityCurve = AnimationCurve.Constant(0f, 1f, 2f),
                StayTime = 1f
            };

            AddWaypointToList(waypointInfo, waypoint);
            Selection.activeTransform = waypoint.transform; 
        }

        void AddWaypointToList(WaypointInfo newLastWaypointInfo, EnvironmentMoverWaypoint newLastWaypoint)
        {
            var waypointsCount = Waypoints.Count;
            var prevWaypointTransform = waypointsCount > 0 ? Waypoints[waypointsCount - 1].Transform : null;
            EnvironmentMoverWaypoint prevWaypoint = null;
            if (prevWaypointTransform)
            {
                prevWaypoint = GetOrAddWaypoint(prevWaypointTransform);
                prevWaypoint.Next = newLastWaypoint;
            }
            
            var nextWaypointTransform = waypointsCount > 0 ? Waypoints[0].Transform : null;
            EnvironmentMoverWaypoint nextWaypoint = null;
            if (nextWaypointTransform)
            {
                nextWaypoint = GetOrAddWaypoint(nextWaypointTransform);
                nextWaypoint.Previous = newLastWaypoint;
            }
            
            newLastWaypoint.SetOrder(waypointsCount, prevWaypoint, nextWaypoint);
            Waypoints.Add(newLastWaypointInfo);
        }

        int GetPathIndex()
        {
            var splitedContainerName = WaypointsContainer.name.Split(' ');
            var pathIndex = int.Parse(splitedContainerName[splitedContainerName.Length - 1]);
            return pathIndex;
        }

        EnvironmentMover GetMover()
        {
            var mover = WaypointsContainer.GetComponentInParent<EnvironmentMover>();
            return mover;
        }
#endif
    }

}
