﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class EnvironmentMoverEntity
{
    public enum State
    {
        Disabled,
        Playing,
        Paused,
        Completed
    }
    
    public string Id;
    public Vector3 SpawnPosition;

    public State CurrentState;
    
    public int PathCount = 0;
    public int PathIndex = 0;

    public List<int> PathsLoopCount;
    public int LoopsPlayed = 0;
    
    public List<int> PathsWaypointsCount;
    public int WaypointIndex = 0;

    public float StopTimeRemained = 0f;

    public float ContainerRotation = 0f;
    public Vector3 ContainerPosition = Vector2.zero;

    public bool IsDisabled => CurrentState == State.Disabled;
    public bool IsPlaying => CurrentState == State.Playing;
    public bool IsPaused => CurrentState == State.Paused;
    public bool IsCompleted => CurrentState == State.Completed;

    int LoopCount => PathsLoopCount[PathIndex];
    int RemainingLoops => LoopCount - LoopsPlayed;
    bool ContinueLooping => LoopCount != -1 ? RemainingLoops > 0 : true;

    EnvironmentMover View;

    public EnvironmentMoverEntity(EnvironmentMover view)
    {
        View = view;
        Id = view.Id;
        
        view.Init(this);
        SpawnPosition = view.transform.position;
    }

    public void Load(EnvironmentMover view)
    {
        View = view;
        view.Init(this);
    }

    public void SavePreparation()
    {
        if (View)
        {
            ContainerPosition = View.ContainerPosition;
            ContainerRotation = View.ContainerRotation;
            StopTimeRemained = View.StopTimeRemained;
        }
        else
        {
            Debug.LogError($"Try to update postion of null View {Id}: {SpawnPosition}");
        }
    }

    public void SetPath(int index)
    {
        if(index >= -1 && index <= PathCount) 
        {
            if(index == -1)
            {
                PathIndex = PathCount - 1;
            }
            else if (index == PathCount)
            {
                PathIndex = 0;
            }
            else
            {
                PathIndex = index;
            }

            LoopsPlayed = 0;
            WaypointIndex = 0;
            StopTimeRemained = 0;
        }
        else
        {
            Debug.LogError($"Invalid path index: {index}");
        }
    }

    public void UpdateWaypoint()
    {
        if (WaypointIndex != PathsWaypointsCount[PathIndex] - 1)
        {
            WaypointIndex++;
        }
        else
        {
            WaypointIndex = 0;
        }

        if (WaypointIndex == 0)
        {
            HandleLoop();
        }
    }

    void HandleLoop()
    {
        LoopsPlayed++;
        if(!ContinueLooping)
        {
            CurrentState = State.Completed;
            View.OnCompleted();
        }
    }

}
