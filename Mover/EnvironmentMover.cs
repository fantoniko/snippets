﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Game.Environment.Mover;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class EnvironmentMover : IdentifiableMonoBehaviour, ICollisionTriggerEnter, ICollisionTriggerExit
{
    const float SpeedEpsilon = 0.01f;
    
    const float DistanceEpsilon = 0.01f;
    const float MoveAngleEpsilon = 1f;

    const float FullTurnEpsilon = 0.01f;
        
    const string WaypointsContainerName = "Waypoints";

    [SerializeField] bool DontChangeRigidbodyType;
    [SerializeField] Rigidbody2D MainObjectRigidBody;

    [Space]
    [SerializeField] List<CollisionTrigger> ContainerTriggers = new List<CollisionTrigger>();
    
    [SerializeField, GUIColor(0.9f, 0.9f, 1f)] List<EnvironmentMoverPath> Paths = new List<EnvironmentMoverPath>();

    EnvironmentMoverEntity Entity;
    SessionEntity Session;

    Rigidbody2D[] ChildRigidbodies;
    List<MovingSurfaceHandler> ActiveHandlers = new List<MovingSurfaceHandler>();

    int LastWaypointMovedIndex = -1;
    bool IsMoving = false;
    bool IsAudioPlaying;
    float StopTime;

    float ContainerStartRotation;
    Vector2 ContainerStartPosition;

    public List<CollisionTrigger> ContainerTriggersPub => ContainerTriggers;
    
    public event Action<MovingSurfaceHandler> MoverZoneTriggered;

    public Vector2 ContainerPosition => MainObjectRigidBody.position;
    public float ContainerRotation => MainObjectRigidBody.rotation;
    public float StopTimeRemained => StopTime > 0f ? StopTime : 0f;

    bool PlayOnStart => Paths[0].PlayOnStart;
    
    int CurrentPathIndex => Entity.PathIndex;
    int NextWaypointIndex => Entity.WaypointIndex;
    int PreviousWaypointIndex => Entity.WaypointIndex - 1;

    float NextWaypointRotation => WaypointRotation(NextWaypointIndex);
    float PreviousWaypointRotation => WaypointRotation(PreviousWaypointIndex);
    float WayRatio => WayContainerDoneVector.magnitude / WayVector.magnitude;

    Vector2 CurrentContainerVelocity => MainObjectRigidBody.velocity;
    Vector2 WayVector => NextWaypointPosition - PreviousWaypointPosition;
    Vector2 WayContainerDoneVector => ContainerPosition - PreviousWaypointPosition;
    Vector2 WayLinearVelocity => CurrentWaypointInfo.MoveTime > 0f ? WayVector / CurrentWaypointInfo.MoveTime : Vector2.zero;
    Vector2 WayCurveVelocity => WayVector.normalized * (CurrentWaypointInfo.VelocityCurve.Evaluate(WayRatio) + SpeedEpsilon);
    Vector2 NextWaypointPosition => WaypointPosition(NextWaypointIndex);
    Vector2 PreviousWaypointPosition => WaypointPosition(PreviousWaypointIndex);

    List<WaypointInfo> CurrentWaypoints => Paths[CurrentPathIndex].Waypoints;
    WaypointInfo CurrentWaypointInfo => CurrentWaypoints[NextWaypointIndex];

    void FixedUpdate()
    {
        HandleSequence();
    }

    public void SetSession(SessionEntity session)
    {
        Session = session;
    }

    public void Init(EnvironmentMoverEntity entity)
    {
        Entity = entity;
        Entity.PathCount = Paths.Count;
        Entity.PathsWaypointsCount = new List<int>(Paths.Select(p => p.Waypoints.Count));
        Entity.PathsLoopCount = new List<int>(Paths.Select(p => p.LoopCount));
        ContainerStartPosition = ContainerPosition;
        ContainerStartRotation = ContainerRotation;

        ChildRigidbodies = GetComponentsInChildren<Rigidbody2D>();

        foreach (var trigger in ContainerTriggers)
        {
            trigger.AddHandlers(this);
        }

        if (Entity.IsDisabled)
        {
            InitMovement();
            if (PlayOnStart)
            {
                PlaySequence();
            }
        }
        else
        {
            LoadSequence();
        }

        if (!Entity.IsPlaying)
        {
            DisableRigidBodySimulation();
        }
    }

    Vector2 WaypointPosition(int waypointIndex)
    {
        Vector2 position;
        if (waypointIndex != -1)
        {
            position = CurrentWaypoints[waypointIndex].Transform.position;
        }
        else if (CurrentWaypoints.Count > 1)
        {
            position = CurrentWaypoints[CurrentWaypoints.Count - 1].Transform.position;
        }
        else
        {
            position = ContainerStartPosition;
        }

        return position;
    }

    float WaypointRotation(int waypointIndex)
    {
        float rotation;
        if (waypointIndex != -1)
        {
            rotation = CurrentWaypoints[waypointIndex].Rotation;
        }
        else if (CurrentWaypoints.Count > 1)
        {
            rotation = CurrentWaypoints[CurrentWaypoints.Count - 1].Rotation;
        }
        else
        {
            rotation = ContainerStartRotation;
        }

        return rotation;
    }

#region Mover Sequence
    public void PlayPath(int pathIndex)
    {
        Entity.CurrentState = EnvironmentMoverEntity.State.Disabled;
        ContainerStartPosition = ContainerPosition;
        UpdatePath(pathIndex);
        PlaySequence();
    }

    public void PlaySequence()
    {
        if(Entity.IsPlaying)
        {
            return;
        }

        EnableRigidBodySimulation();
        Entity.CurrentState = EnvironmentMoverEntity.State.Playing;
    }

    public void PauseSequence()
    {
        if(Entity.IsPaused)
        {
            return;
        }

        SetContainerVelocity(Vector2.zero);
        DisableRigidBodySimulation();
        Entity.CurrentState = EnvironmentMoverEntity.State.Paused;
    }

    public void OnCompleted()
    {
        DisableRigidBodySimulation();
    }

    void InitMovement()
    {
        EnableRigidBodySimulation();
        if(CurrentWaypoints.Count > 1)
        {
            TeleportToWaypoint(0);
            Entity.WaypointIndex = 1;
        }
        LastWaypointMovedIndex = -1;
    }

    void LoadSequence()
    {
        SetContainerPosition(Entity.ContainerPosition);
        SetContainerRotation(Entity.ContainerRotation, false);
      
        if(Entity.StopTimeRemained >= 0f)
        {
            StopTime = Entity.StopTimeRemained;
        }
    }

    void EnableRigidBodySimulation()
    {
        if(DontChangeRigidbodyType) {return;}
        MainObjectRigidBody.bodyType = RigidbodyType2D.Kinematic;
    }

    void DisableRigidBodySimulation()
    {
        if(DontChangeRigidbodyType) {return;}
        MainObjectRigidBody.bodyType = RigidbodyType2D.Static;
    }

    [PropertyOrder(2)]
    [ResponsiveButtonGroup, Button(ButtonHeight = 20, Name = nameof(ToggleSequence)), GUIColor(0.3f, 0.5f, 0.8f)]
    public void ToggleSequence()
    {
        if(Entity.IsPlaying)
        {
            PauseSequence();
        }
        else
        {
            PlaySequence();
        }
    }

    [PropertyOrder(1)]
    [ResponsiveButtonGroup, Button(ButtonHeight = 20, Name = nameof(SetPreviousPath)), GUIColor(0.7f, 0.3f, 0.8f)]
    public void SetPreviousPath()
    {
        UpdatePath(Entity.PathIndex - 1);
    }

    [PropertyOrder(3)]
    [ResponsiveButtonGroup, Button(ButtonHeight = 20, Name = nameof(SetNextPath)), GUIColor(0.7f, 0.3f, 0.8f)]
    public void SetNextPath()
    {
        UpdatePath(Entity.PathIndex + 1);
    }

    void UpdatePath(int pathIndex)
    {
        Entity.SetPath(pathIndex);
        InitMovement();
        if(Entity.IsPlaying)
        {
            SetMoving();
        }
    }

    void HandleSequence()
    {
        if (Entity != null && Entity.IsPlaying)
        {
            if (IsMoving)
            {
                HandleMoving();
            }
            else
            {
                HandleStopTimer();
            }
        }
    }

    void HandleMoving()
    {
        CheckStartActions();

        var movingVector = NextWaypointPosition - ContainerPosition;
        var distance = movingVector.magnitude;

        var movingIsDone = distance < DistanceEpsilon 
                           || Vector2.Angle(CurrentContainerVelocity, movingVector) > MoveAngleEpsilon;

        var rotationDelta = NextWaypointRotation - PreviousWaypointRotation;
        var rotationIsDone =  Mathf.Abs(rotationDelta) < MoveAngleEpsilon
                                || (Mathf.Abs(rotationDelta) % 360f < FullTurnEpsilon && CurrentWaypointInfo.MoveTime == 0f)
                                || (rotationDelta > 0f && Entity.ContainerRotation >= NextWaypointRotation)
                                || (rotationDelta < 0f && Entity.ContainerRotation <= NextWaypointRotation);
        
        var wayIsDone = movingIsDone && rotationIsDone;
        
        if(wayIsDone)
        {
            OnWaypointReached();
        }

        if (!movingIsDone)
        {
            InterpolateVelocity();
        }

        if (!rotationIsDone)
        {
            InterpolateRotation();
        }
    }

    void InterpolateVelocity()
    {
        if (CurrentWaypointInfo.UseVelocityCurve)
        {
            SetContainerVelocity(WayCurveVelocity);
        }
        else
        {
            SetContainerVelocity(WayLinearVelocity);
        }
    }

    void InterpolateRotation()
    {
        var waypointsRotation = NextWaypointRotation - PreviousWaypointRotation;
        var moveTime = CurrentWaypointInfo.MoveTime;
        var newRotation = moveTime > Time.fixedDeltaTime 
            ? Entity.ContainerRotation + waypointsRotation * Time.fixedDeltaTime / moveTime
            : NextWaypointRotation;
        SetContainerRotation(newRotation, true);
    }

    void HandleStopTimer()
    {
        StopTime -= Time.fixedDeltaTime;
        if(StopTime <= 0f)
        {
            SetMoving();
        }
    }

    void SetContainerVelocity(Vector2 velocity)
    {
        MainObjectRigidBody.velocity = velocity;
        foreach (var rigidbody in ChildRigidbodies)
        {
            if (rigidbody.bodyType != RigidbodyType2D.Static)
            {
                rigidbody.velocity = velocity;    
            }
        }
        UpdateHandlersVelocity(velocity);
    }

    void SetContainerRotation(float newRotation, bool enableAngleVelocity)
    {
        var oldRotation = Entity.ContainerRotation;
        Entity.ContainerRotation = newRotation;
        MainObjectRigidBody.MoveRotation(newRotation);

        var deltaRotation = enableAngleVelocity ? newRotation - oldRotation : 0f;
        UpdateHandlersAngleVelocity(deltaRotation);
    }

    void SetContainerPosition(Vector2 position)
    {
        MainObjectRigidBody.position = position;
    }

    void TeleportToWaypoint(int waypointIndex)
    {
        SetContainerPosition(WaypointPosition(waypointIndex));
        SetContainerVelocity(Vector2.zero);
        SetContainerRotation(WaypointRotation(waypointIndex), false);
    }

    void OnWaypointReached()
    {
        CheckReachActions();
        TeleportToWaypoint(NextWaypointIndex); 
        ResetMoving(CurrentWaypointInfo.StayTime);

        Entity.UpdateWaypoint();
        HandleSequence();
    }

    void CheckStartActions()
    {
        if (LastWaypointMovedIndex != NextWaypointIndex)
        {
            if (CurrentWaypointInfo.Transform.TryGetComponent(out EnvironmentMoverWaypoint waypoint) 
                && waypoint.OnStartAction != null)
            {
                waypoint.OnStartAction?.Handle(Session);
            }
            else
            {
                CurrentWaypointInfo.OnStartAction?.Handle(Session);
            }
        }
        LastWaypointMovedIndex = NextWaypointIndex;
    }

    void CheckReachActions()
    {
        if (CurrentWaypointInfo.Transform.TryGetComponent(out EnvironmentMoverWaypoint waypoint)
            && waypoint.OnReachedAction != null)
        {
            waypoint.OnReachedAction?.Handle(Session);
        }
        else
        {
            CurrentWaypointInfo.OnReachedAction?.Handle(Session);
        } 
    }

    void SetMoving()
    {
        EnableRigidBodySimulation();
        IsMoving = true;
    }

    void ResetMoving(float newStayTime)
    {
        if (newStayTime > 0f)
        {
            DisableRigidBodySimulation();
            IsMoving = false;
            StopTime = newStayTime;
        }
    }
    
    #endregion

#region Trigger Handling
    void UpdateHandlersVelocity(Vector2 newVelocity)
    {
        foreach (var handler in ActiveHandlers)
        {
            handler.SetVelocityGlobal(newVelocity);
        }
    }
    
    void UpdateHandlersAngleVelocity(float deltaRotation)
    {
        foreach (var handler in ActiveHandlers)
        {
            handler.SetAngleVelocity(ContainerPosition, deltaRotation);
        }
    }

    public bool AddHandler(MovingSurfaceHandler handler)
    {
        bool result = !ActiveHandlers.Contains(handler);
        if (result)
        {
            ActiveHandlers.Add(handler);
            handler.SetVelocityGlobal(CurrentContainerVelocity);
        }
        return result;
    }

    public bool RemoveHandler(MovingSurfaceHandler handler)
    {
        bool result = ActiveHandlers.Contains(handler);
        if (result)
        {
            ActiveHandlers.Remove(handler);
            handler.SetVelocityGlobal(Vector2.zero);
            handler.SetAngleVelocity(Vector2.zero);
        }
        return result;
    }

    void ICollisionTriggerEnter.HandleEnter(CollisionTrigger trigger, Collider collider, GameObject root)
    {
        var foundHandler = GetMovingHandler(collider);
        if (foundHandler && ContainerTriggers.Contains(trigger))
        {
            AddHandler(foundHandler);
            MoverZoneTriggered?.Invoke(foundHandler);
        }
    }

    void ICollisionTriggerExit.HandleExit(CollisionTrigger trigger, Collider collider, GameObject root)
    {
        var foundHandler = GetMovingHandler(collider);
        if (foundHandler && ContainerTriggers.Contains(trigger))
        {
            RemoveHandler(foundHandler);
        }
    }

    MovingSurfaceHandler GetMovingHandler(Collider collider)
    {
        var movingHandler = collider.GetComponent<MovingSurfaceHandler>();
        return movingHandler;
    }
#endregion

#region Editor Tools

#if UNITY_EDITOR
    
    void OnDrawGizmos()
    {
        if (!SceneViewUpdater.IsPointInsideSceneView(ContainerPosition))
        {
            return;
        }
        
        var cachedGizmosColor = Gizmos.color;
        
        var iconPosition = ContainerPosition;
        Gizmos.DrawIcon(iconPosition, "EnvironmentMover/MoverContainerIcon.png", true);

        Gizmos.color = Color.white;
        var firstWaypointTransform = Paths[0].Waypoints[0].Transform;
        if (firstWaypointTransform)
        {
            var firstWaypointPosition = firstWaypointTransform.position;
            Gizmos.DrawLine(ContainerPosition, firstWaypointPosition);    
        }

        Gizmos.color = cachedGizmosColor;
    }

#endif

#endregion
}
