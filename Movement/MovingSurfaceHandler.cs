﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingSurfaceHandler : MonoBehaviour
{
    public Vector2 VelocityGlobal { get; private set; }
    public Vector2 VelocityRelative { get; private set; }
    public Vector2 VelocityImpact { get; private set; }
    public Vector2 AngleVelocity { get; private set; }

    public Vector2 Velocity => Rigidbody.velocity;
    public bool HasGlobalVelocity => VelocityGlobal != Vector2.zero;

    Rigidbody2D CachedRigidbody;
    public Rigidbody2D Rigidbody
    {
        get
        {
            if (!CachedRigidbody)
            {
                CachedRigidbody = GetComponent<Rigidbody2D>();
            }
            return CachedRigidbody;
        }
    }

    void FixedUpdate()
    {
        UpdateRelative();
    }

    public void SetVelocityRelative(Vector2 velocity)
    {
        VelocityRelative = velocity;
        UpdateVelocity();
    }

    public void SetVelocityImpact(Vector2 velocity)
    {
        VelocityImpact = velocity;
        UpdateVelocity();
    }

    public void SetVelocityGlobal(Vector2 velocity)
    {
        VelocityGlobal = velocity;
        UpdateVelocity();
    }

    public void SetAngleVelocity(Vector2 pivotPoint, float deltaRotation)
    {
        var rootToBodyVector = Rigidbody.position - pivotPoint;
        var shiftedFromPivotPos = MathHelper.RotateVector(rootToBodyVector, deltaRotation);
        var angleVelocity = (shiftedFromPivotPos - rootToBodyVector) / Time.fixedDeltaTime;
        SetAngleVelocity(angleVelocity);
    }

    public void SetAngleVelocity(Vector2 velocity)
    {
        AngleVelocity = velocity;
        UpdateVelocity();
    }

    public void AddForceRelative(Vector2 force, ForceMode2D mode = ForceMode2D.Impulse)
    {
        Rigidbody.AddForce(force, mode);
        UpdateRelative();
    }

    void UpdateRelative()
    {
        VelocityRelative = Rigidbody.velocity - VelocityGlobal - VelocityImpact - AngleVelocity;
    }

    void UpdateVelocity()
    {
        Rigidbody.velocity = VelocityRelative + VelocityGlobal + VelocityImpact + AngleVelocity;
    }
}
