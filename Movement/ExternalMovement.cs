﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExternalMovement
{
    MovingSurfaceHandler Handler;
    EnvironmentMover TargetMover;
    bool IsSet;

    public ExternalMovement(MovingSurfaceHandler handler)
    {
        Handler = handler;
        IsSet = false;
    }

    public void Set(Component moverChild)
    {
        var mover = moverChild?.GetComponentInParent<EnvironmentMover>();
        if(mover)
        {
            TargetMover = mover;
            IsSet = mover.AddHandler(Handler);
            
            if (IsSet)
            {
                TargetMover.MoverZoneTriggered += Reset;    
            }
        }
    }

    public void Unset()
    {
        if (!IsSet)
        {
            return;
        }

        TargetMover.MoverZoneTriggered -= Reset;
        TargetMover.RemoveHandler(Handler);

        TargetMover = null;
        IsSet = false;
    }

    void Reset(MovingSurfaceHandler handler)
    {
        if (handler == Handler)
        {
            IsSet = false;
            TargetMover.MoverZoneTriggered -= Reset;
        }
    }
}
